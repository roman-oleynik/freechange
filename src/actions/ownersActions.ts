import {Action, Thing, Owner, Photo, SearchBarState, ChatObject, MessageObject} from '../types/types';
import {
    SET_OWNER, 
    SET_LOGGED_OWNER
} from '../types/constants';
import { Dispatch, AnyAction } from 'redux';
import { database, storage } from '../firebase/firebaseConfig';
import { ThunkAction } from 'redux-thunk';
import { isURL } from '../modules/isURL';


export function setOwner(owner: Owner): Action {
    return {
        type: SET_OWNER,
        body: owner
    };
};

export function fetchOwner(id: string) {
    return (dispatch: any) => {
        return fetch(`https://freechange.firebaseio.com/owners.json/`)
            .then(res => res.json())
            .then(data => {
                for (let key of Object.keys(data)) {
                    const owner = data[key];
                    if (owner.id === id) {
                        dispatch(setOwner(owner));
                        break;
                    };
                }
            })
    }
};

export function setLoggedOwner(loggedOwner: Owner | null): Action {
    return {
        type: SET_LOGGED_OWNER,
        body: loggedOwner
    };
};


export function fetchLoggedOwner(loggedOwnerId: string) {
    return async (dispatch: Dispatch<Action>): Promise<Action> => {
        return fetch(`https://freechange.firebaseio.com/owners.json/`)
            .then(res => res.json())
            .then(data => {
                for (let key of Object.keys(data)) {
                    const owner = data[key];
                    if (owner.id === loggedOwnerId) {
                        return owner;
                    };
                }
            })
            .then(owner => dispatch(setLoggedOwner(owner)));
    }
}

export function addOwner(ownerPayload: Owner) {
    const newKey = database.ref().child('things').push().key;
    ownerPayload.key = newKey as string;
    return async (dispatch: Dispatch<Action>): Promise<void> => {
        return database.ref(`/owners/${newKey}`).update(ownerPayload);
    }
};

export function editOwner(key: string, payload: Owner, avatar: any) {
    return async (dispatch: Dispatch<any>) => {
        if (isURL(avatar)) {
            payload.avatar = avatar;
        }
        return database.ref(`/owners/${key}`).update(payload)
        .then(() => {
            if (!isURL(avatar)) {
                const uploadTask = storage.ref(`/avatar/${key}`).putString(avatar, "data_url");

                uploadTask.on('state_changed',
                    (snapshot) => {
                    }, 
                    (error) => {
                        // Handle unsuccessful uploads
                    }, 
                    () => {
                        // Handle successful uploads on complete
                        // For instance, get the download URL: https://firebasestorage.googleapis.com/...
                        storage.ref('avatar').child(key).getDownloadURL()
                        .then(function(downloadURL) {
                            payload.avatar = downloadURL;
                            database.ref(`/owners/${key}`).update(payload);
                            console.log(payload);
                        })
                        .then(() => dispatch(fetchLoggedOwner(payload.id)))
                    }
                );
            } else {
                dispatch(fetchLoggedOwner(payload.id));
            }
        })
    }
};
