import {Action, Thing, Owner, Photo, SearchBarState, ChatObject, MessageObject} from '../types/types';
import {
    SET_THINGS, 
    SET_THING, 
    TOGGLE_IS_FREE_CHECKBOX_VALUE,
    SET_THING_NAME_INPUT_VALUE,
    SET_VISIBLE_THINGS,
    SET_FILTERED_THINGS,
    SET_VISIBLE_FILTERED_THINGS
} from '../types/constants';
import { Dispatch, AnyAction } from 'redux';
import { database } from '../firebase/firebaseConfig';
import { ThunkAction } from 'redux-thunk';
import { storage } from '../firebase/firebaseConfig';
import { generateId } from '../modules/generateId';
import { isURL } from '../modules/isURL';
import { addPhoto } from './photosActions';



export function setThings(things:Thing[]): Action {
    return {
        type: SET_THINGS,
        body: things
    };
};
export function setVisibleThings(things:Thing[]): Action {
    return {
        type: SET_VISIBLE_THINGS,
        body: things
    };
};
export function setFilteredThings(things:Thing[]): Action {
    return {
        type: SET_FILTERED_THINGS,
        body: things
    };
};
export function setVisibleFilteredThings(things: Thing[], position: number, amount: number): Action {
    return {
        type: SET_VISIBLE_FILTERED_THINGS,
        body: things,
        position,
        amount
    };
};
export function setThing(thing: Thing): Action {
    return {
        type: SET_THING,
        body: thing
    };
};
export function fetchThings() {
    return async (dispatch: Dispatch<any>): Promise<void> => {
        return fetch(`https://freechange.firebaseio.com/things.json`)
        .then((response => response.json()))
        .then((data) => {
            dispatch(setThings(data));
        })
    }
};



function getStartKey(keys: string[], fromPoint: number): string {
    let result: string = "";
    keys.forEach((key: string, i: number) => {
        if (i === fromPoint) {
            result = key;
        }
    });
    return result;
}

export function fetchVisibleThings(things: Thing[], from: number, amount: number) {
    return async (dispatch: Dispatch<Action>): Promise<void> => {
            const keysArray: string[] = things.map((thing: Thing) => thing.key).sort();
            
            let fromKey: string = getStartKey(keysArray, from);

            return fetch(`https://freechange.firebaseio.com/things.json?orderBy="key"&startAt="${fromKey}"&limitToFirst=5`)
                .then(response => response.json())
                .then((data) => {
                    dispatch(setVisibleThings(data));
                })
                .catch(err => console.log(err));

        }
};



export function addThing(payload: Thing, photos: Photo[]) {
    return async (dispatch: Dispatch<any>) => {
        const newThingKey = database.ref().child('things').push().key;
        payload.key = newThingKey as string;
        dispatch(updateThing(payload, photos));
    }
};


function assignRelativeThingToPhotos(rel_Thing: string, photos: Photo[]) {
    photos.forEach((photo: Photo) => photo.rel_Thing = rel_Thing);
};
async function setThingDataExceptPhotosToDB(key: string, payload: Thing) {
    return database.ref(`/things/${key}`).update(payload);
};
async function getAvatarFromFirebaseStorage(key: string) {
    return storage.ref('photos').child(key).getDownloadURL();
};
async function updateThingObjectWithAvatar(payload: Thing) {
    return database.ref(`/things/${payload.key}`).update(payload);
};
async function uploadRestPhotos(photos: Photo[], payload: Thing) {
    for await (let photo of photos) {
        addPhoto(photo, payload);
    }
};


export function updateThing(payload: Thing, photos: Photo[]) {
    return async (dispatch: Dispatch<any>) => {
        if (photos.length) {
            assignRelativeThingToPhotos(payload.id, photos);
            
            return setThingDataExceptPhotosToDB(payload.key, payload)
                .then(() => {
                    const firstPhotoAsAvatar = photos[0];
                    return addPhoto(firstPhotoAsAvatar, payload);
                })
                .then(() => {
                    return getAvatarFromFirebaseStorage(photos[0].id);
                })
                .then((avatarURL) => {
                    payload.avatar = avatarURL;
                    return updateThingObjectWithAvatar(payload);
                })
                .then(() => {
                    dispatch(fetchThings());
                })
                .then(() => {
                    if (photos.length > 1) {
                        const restPhotos = photos.slice(1);
                        uploadRestPhotos(restPhotos, payload);
                    } else {
                        return;
                    }
                })
                .catch(err => console.log(err));
        }
        else {
            return setThingDataExceptPhotosToDB(payload.key, payload)
                .then(() => {
                    dispatch(fetchThings());
                })
                .catch(err => console.log(err));
        }
        
    }
};

export function deleteThing(thingKey: string) {
    return async (dispatch: Dispatch<Action>): Promise<void> => {
        return database.ref(`/things/${thingKey}`).remove();
    }
};

export function setThingNameInputValue(inputValue: string): Action {
    return {
        type: SET_THING_NAME_INPUT_VALUE,
        body: inputValue
    };
};



export function setIsFreeCheckboxValue(value: boolean): Action {
    return {
        type: TOGGLE_IS_FREE_CHECKBOX_VALUE,
        body: value
    };
};



function getArrayFromObject(object: any): Thing[] {
    return Object.keys(object).map((key: string) => {
        return object[key];
    });
};

function selectFreeThings(array: Thing[]): Thing[] {
    return array.filter((thing: Thing) => {
        return Number(thing.price) === 0;
    })
};

function filterThingsByValue(things: Thing[], value: string): Thing[] {
    return things.filter((thing: Thing) => {
        return thing.name.indexOf(value) === 0;
    })
}

export function filterDataOnSearch(config: SearchBarState) {
    return async (dispatch: Dispatch<any>): Promise<void> => {
        const firstLetter = config.inputValue[0];
        let followingLetter = "";
        if (Boolean(firstLetter)) {
            followingLetter = String.fromCharCode(firstLetter.charCodeAt(0) + 1);
        }
        
        return fetch(`https://freechange.firebaseio.com/things.json?orderBy="name"&startAt="${firstLetter}"&endAt="${followingLetter}"`)
            .then((response) => response.json())
            .then((data) => {
                let thingsArrayFromObject = getArrayFromObject(data);
                if (config.isChecked) {
                    thingsArrayFromObject = selectFreeThings(thingsArrayFromObject);
                }
                const filteredThingsArray: Thing[] = filterThingsByValue(thingsArrayFromObject, config.inputValue);
                dispatch(setFilteredThings(filteredThingsArray));
                dispatch(setVisibleFilteredThings(filteredThingsArray, 0, 5));
            })
            .catch(err => console.log(err));
    }
}

export function discardParametersOfThingsSearching() {
    return async (dispatch: Dispatch<any>): Promise<void> => {
        return fetch(`https://freechange.firebaseio.com/things.json`)
            .then((response) => response.json())
            .then((data) => {
                const thingsArrayFromObject = getArrayFromObject(data);
                dispatch(setVisibleFilteredThings([], 0, 5));
                dispatch(setFilteredThings(thingsArrayFromObject));
                dispatch(fetchVisibleThings(thingsArrayFromObject, 0, 5));
            })
            .catch(err => console.log(err));
    }
}

export function fetchThing(id: string) {
    return async (dispatch: Dispatch<Action>): Promise<void> => {
        return fetch(`https://freechange.firebaseio.com/things.json`)
            .then(res => res.json())
            .then(data => {
                for (let key of Object.keys(data)) {
                    const thing = data[key];
                    if (thing.id === id) {
                        dispatch(setThing(thing));
                        break;
                    };
                }
            })
    }
}