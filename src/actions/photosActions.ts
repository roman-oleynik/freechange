import {Action, Thing, Owner, Photo, SearchBarState, ChatObject, MessageObject} from '../types/types';
import {
    SET_THINGS, 
    SET_THING, 
    SET_OWNER, 
    SET_PHOTOS, 
    TOGGLE_IS_FREE_CHECKBOX_VALUE,
    SET_THING_NAME_INPUT_VALUE,
    FILTER_THINGS_BY_SEARCH,
    SET_LOGGED_OWNER,
    FILTER_CONTACTS,
    FILTER_MESSAGES
} from '../types/constants';
import { Dispatch, AnyAction } from 'redux';
import { database, storage } from '../firebase/firebaseConfig';
import { ThunkAction } from 'redux-thunk';
import { isURL } from '../modules/isURL';


export function setPhotos(photos: Photo[]): Action {
    return {
        type: SET_PHOTOS,
        body: photos
    };
};

export function fetchPhotos(thingId: string) {
    return (dispatch: Dispatch<Action>): Promise<void> => {
        return fetch(`https://freechange.firebaseio.com/photos.json`)
            .then(res => res.json())
            .then(data => {
                let matchedPhotos: Photo[] = []; 

                for (let key of Object.keys(data)) {
                    const photo = data[key];
                    if (photo.rel_Thing === thingId) {
                        matchedPhotos.push(photo);
                    };
                }
                // write the filtering action 
                dispatch(setPhotos(matchedPhotos));
            })
    }
}

export async function addPhoto(photo: Photo, payload: Thing) {
    async function addPhotoToFirebaseStorage(photoId: string, photoSrc: string) {
        return storage.ref(`/photos/${photoId}`).putString(photoSrc, "data_url");
    }
    async function getDownloadURLFromFirebaseStorage(key: string) {
        return storage.ref('photos').child(key).getDownloadURL();
    }
    async function addNewPhotoToDatabase(key: string, photo: Photo) {
        return database.ref(`/photos/${key}`).update(photo);
    }

    if (!isURL(photo.source)) {

        await addPhotoToFirebaseStorage(photo.id, photo.source);

        getDownloadURLFromFirebaseStorage(photo.id)
            .then((downloadURL) => {
                photo.source = downloadURL;
                const newPhotoKey = database.ref().child('photos').push().key;
                photo.key = newPhotoKey as string;
                return newPhotoKey;
            })
            .then((key: string | null) => {
                if (typeof key === "string") {
                    return addNewPhotoToDatabase(key, photo);
                } else {
                    throw new Error("Type of the key of the new Photo object is not 'string'.");
                }
            })
            .catch((err) => console.log(err));
    } else {
        return;
    }
}




