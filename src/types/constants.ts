export const SET_THINGS = "SET_THINGS";
export const SET_THING = "SET_THING";
export const SET_OWNER = "SET_OWNER";
export const SET_PHOTOS = "SET_PHOTOS";
export const FILTER_CONTACTS = "FILTER_CONTACTS";
export const FILTER_MESSAGES = "FILTER_MESSAGES";
export const TOGGLE_IS_FREE_CHECKBOX_VALUE = "TOGGLE_IS_FREE_VALUE";
export const SET_THING_NAME_INPUT_VALUE = "SET_THING_NAME_INPUT_VALUE";
export const FILTER_THINGS_BY_SEARCH = "FILTER_THINGS_BY_SEARCH";
export const SET_LOGGED_OWNER = "SET_LOGGED_OWNER";
export const ADD_OWNER = "ADD_OWNER";
export const SET_VISIBLE_THINGS = "SET_VISIBLE_THINGS";
export const SET_FILTERED_THINGS = "SET_FILTERED_THINGS";
export const SET_VISIBLE_FILTERED_THINGS = "SET_VISIBLE_FILTERED_THINGS";