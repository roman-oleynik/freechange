export type Action = {
    type: string,
    body?: any,
    filterCriteria?: any,
    things?: Thing[],
    position?: number,
    amount?: number
};
export type Thing = {
    id: string,
    name: string,
    description: string,
    avatar: string,
    rel_Owner: string,
    price: number,
    currency: string,
    key: string
};
export type MessageObject = {
    id: string,
    createdAt: string,
    rel_Chat: string, // Chat.id
    rel_Owner: Owner, // Owner.id
    text: string
};
export type ChatObject = {
    id: string,
    owners: Array<Owner>,
    messages?: MessageObject[]
};
export type Owner = {
    id: string,
    key: string,
    name: string,
    email: string,
    location: string
    avatar: string
};
export type Photo = {
    id: string,
    key: string,
    source: string,
    rel_Thing: string
};
export type SearchBarState = {
    inputValue: string,
    isChecked: boolean
};
export type State = {
    loggedOwner: Owner | null,
    things: Thing[],
    visibleThings: Thing[],
    filteredThings: Thing[],
    visibleFilteredThings: Thing[],
    thing: Thing,
    owner: Owner,
    thingPhotos: Photo[],
    searchBarState: SearchBarState,
    chats: ChatObject[],
    messages: MessageObject[]
};