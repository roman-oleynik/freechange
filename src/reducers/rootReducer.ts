import {Action, SearchBarState, State, ChatObject, MessageObject} from '../types/types';
import {combineReducers} from 'redux';
import {Thing, Owner, Photo} from '../types/types';
import {
    SET_THINGS, 
    SET_THING, 
    SET_OWNER, 
    SET_PHOTOS, 
    TOGGLE_IS_FREE_CHECKBOX_VALUE, 
    SET_THING_NAME_INPUT_VALUE,
    FILTER_THINGS_BY_SEARCH,
    SET_LOGGED_OWNER,
    FILTER_MESSAGES,
    FILTER_CONTACTS,
    SET_VISIBLE_THINGS,
    SET_FILTERED_THINGS,
    SET_VISIBLE_FILTERED_THINGS
} from '../types/constants';
import { polishString } from '../modules/polishString';
import { act } from 'react-dom/test-utils';

let initialStateOfLoggedOwner: Owner | null = null;
let initialStateOfThings: Thing[] = [];
let initialStateOfThing: Thing = {} as Thing;
let initialStateOfOwner: Owner = {} as Owner;
let initialStateOfPhotos: Photo[] = [] as Photo[];
let initialStateOfSearchBar: SearchBarState = {
    inputValue: "",
    isChecked: false
};
let initialStateOfChats: ChatObject[] = [];
let initialStateOfMessages: MessageObject[] = [];



function thingsReducer(state = initialStateOfThings, action: Action) {
    switch(action.type) {
        case SET_THINGS:
            const thingsArray: Array<Thing> = [];
            for (let key in action.body) {
                action.body[key].key = key;
                thingsArray.push(action.body[key]);
            };
            return thingsArray;
        default:
            return state;
    }
};

function visibleThingsReducer(state = initialStateOfThings, action: Action) {
    switch(action.type) {
        case SET_VISIBLE_THINGS:
            const thingsArray: Array<Thing> = [];
            for (let key in action.body) {
                action.body[key].key = key;
                thingsArray.push(action.body[key]);
            };
            return thingsArray;
        default:
            return state;
    }
}

function filteredThingsReducer(state = initialStateOfThings, action: Action) {
    switch(action.type) {
        case SET_FILTERED_THINGS:
            return [...action.body];
        case FILTER_THINGS_BY_SEARCH:
            const filteredThings: Thing[] = (action.things as Thing[]).filter((thing: Thing) => {
                const inputValue: string = polishString(action.body.inputValue);
                const thingName: string = polishString(thing.name);
                
                return thingName.indexOf(inputValue) !== -1;
            });
            if (action.body.isChecked) {
                return filteredThings && filteredThings.filter((thing: Thing) => {
                    console.log(thing.price);
                    return Number(thing.price) === 0;
                });
            } else {
                return filteredThings;
            }
        default:
            return state;
    }
};



function visibleFilteredThingsReducer(state = initialStateOfThings, action: Action) {
    switch(action.type) {
        case SET_VISIBLE_FILTERED_THINGS:
            const pageContent = action.body.slice(action.position, (action.position as number) + (action.amount as number))
            return pageContent;
        default:
            return state;
    }
}

function thingReducer(state = initialStateOfThing, action: Action) {
    switch(action.type) {
        case SET_THING:
            return {...action.body};
        default:
            return state;
    }
}


function ownerReducer(state = initialStateOfOwner, action: Action) {
    switch(action.type) {
        case SET_OWNER:
            return {...action.body};
        default:
            return state;
    }
};

function thingPhotosReducer(state = initialStateOfPhotos, action: Action) {
    switch(action.type) {
        case SET_PHOTOS:
            return [...action.body];
        default:
            return state;
    }
};

function searchBarStateReducer(state = initialStateOfSearchBar, action: Action) {
    switch(action.type) {
        case TOGGLE_IS_FREE_CHECKBOX_VALUE:
            return {...state, isChecked: action.body};
        case SET_THING_NAME_INPUT_VALUE:
            return {...state, inputValue: action.body};
        case FILTER_THINGS_BY_SEARCH:
            return {...action.body};
        default:
            return state;
    }
};
function loggedOwnerReducer(state = initialStateOfLoggedOwner, action: Action) {
    switch(action.type) {
        case SET_LOGGED_OWNER:
            if (action.body === null) {
                return null;
            }
            return {...action.body};
        default:
            return state;
    }
};

function chatsReducer(state = initialStateOfChats, action: Action) {
    switch(action.type) {
        case FILTER_CONTACTS:
            if (action.body.length) {
                return action.body.filter((chat: ChatObject) => {
                    const loggedOwner: Owner = action.filterCriteria as Owner;
                    const firstOwnerId: string = chat.owners[0].id;
                    const secondOwnerId: string = chat.owners[1].id;
                    return loggedOwner.id === firstOwnerId || 
                           loggedOwner.id === secondOwnerId;
                });
            } else {
                return state;
            }
            
        default:
            return state;
    }
};

function messagesReducer(state = initialStateOfMessages, action: Action) {
    switch(action.type) {
        case FILTER_MESSAGES:
            return action.body.filter((message: MessageObject) => {
                return message.rel_Chat === action.filterCriteria;
            });
        default:
            return state;
    }
};



export const rootReducer = combineReducers<State>({
    loggedOwner: loggedOwnerReducer,
    things: thingsReducer,
    visibleThings: visibleThingsReducer,
    filteredThings: filteredThingsReducer,
    visibleFilteredThings: visibleFilteredThingsReducer,
    thing: thingReducer,
    owner: ownerReducer,
    thingPhotos: thingPhotosReducer,
    searchBarState: searchBarStateReducer,
    chats: chatsReducer,
    messages: messagesReducer
});