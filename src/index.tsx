import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {Provider} from 'react-redux';
import {store} from './store/store';
import * as serviceWorker from './serviceWorker';
import OnlineCheckerHOC from './components/OnlineCheckerHOC/OnlineCheckerHOC';

ReactDOM.render(<Provider store={store}><OnlineCheckerHOC /></Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
