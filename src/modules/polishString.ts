export function polishString(string: string):string {
    return string.trim().toLowerCase();
}