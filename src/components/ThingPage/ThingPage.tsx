import React, {useState, useEffect} from 'react';
import {fetchThing, setThing} from '../../actions/thingsActions';
import {Thing, Action, SearchBarState} from '../../types/types';
import {connect} from 'react-redux';
import {NavLink} from 'react-router-dom';
import PhotosCarousel from '../PhotosCarousel/PhotosCarousel';
import './ThingPage.scss';
import { ThunkDispatch } from 'redux-thunk';
import { bindActionCreators } from 'redux';

type Match = {
    path: string,
    url: string,
    isExact: boolean,
    params: {
      thingId: string
    }
};

interface StateProps {
    thing: Thing,
};

interface OwnProps {
	match: Match
}

interface DispatchProps {
    fetchThing: (thingId: string) => void
    setThing: (thing: Thing) => void
}

type Props = OwnProps & DispatchProps & StateProps;


interface IState {
    isLoading: boolean
}

class ThingPage extends React.Component<Props, IState> {
    public state: IState = {
        isLoading: true
    };
    public thingId: string = this.props.match.params.thingId;
    componentDidMount = async () => {
        await this.props.fetchThing(this.thingId);
        this.setState({
            isLoading: false
        });
    }
    componentWillUnmount = () => {
        this.props.setThing({} as Thing);
    };
    render() {
        const {isLoading} = this.state;
        if (isLoading) {
            return <div>Loading...</div>
        } else {
            return (
                <div>
                    <h1>{this.props.thing.name}</h1>
                    <PhotosCarousel />
                    <NavLink to={`/owner/${this.props.thing.rel_Owner}`}>Go to the owner's page</NavLink>
                </div>
            );
        }
    }
}

// function ThingPage(props: IProps) {
//     const [isLoading, setIsLoading] = useState(true);

//     const thingId: string = props.match.params.thingId;

//     useEffect(() => {
//         props.dispatch(fetchThing(thingId));
//         setIsLoading(false);
//         return () => {
//             props.dispatch(setThing({} as Thing));

//         }
//     }, [thingId]);

//     if (isLoading) {
//         console.log(isLoading)
//         return <div>Loading...</div>
//     } else {
//         console.log(isLoading)

//         return (
//             <div>
//                 <h1>{props.thing.name}</h1>
//                 <NavLink to={`/owner/${props.thing.rel_Owner}`}>Go to the owner's page</NavLink>
//             </div>
//         );
//     }
    
// }

const mapStateToProps = (state: any) => (
    {
        thing: state.thing,
    }
);

const mapDispatchToProps = (dispatch: ThunkDispatch<{}, {}, Action>) => bindActionCreators(
    {
        fetchThing,
        setThing,
    },
        dispatch
);


export default connect(mapStateToProps, mapDispatchToProps)(ThingPage);