import React, {useState, useRef, MutableRefObject, RefObject, Dispatch, Profiler} from 'react';
import { NavLink, Redirect } from 'react-router-dom';
import {auth, firebase} from "../../firebase/firebaseConfig";
import {fetchLoggedOwner, addOwner} from '../../actions/ownersActions';
import { connect, MapDispatchToPropsParam } from 'react-redux';
import { State, Action, Owner } from '../../types/types';
import { bindActionCreators, AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { setLocalStorageItem } from '../../modules/localStorageAPI';
import './LoginPopup.scss';


interface StateProps {};

interface OwnProps {
}

interface DispatchProps {
	fetchLoggedOwner: (loggedOwnerId: string) => void
	addOwner: (owner: Owner) => void
}

type Props = OwnProps & DispatchProps & StateProps;



function LoginPopup(props: Props) {
	const emailRef = useRef<HTMLInputElement>(null); 
	const passwordRef = useRef<HTMLInputElement>(null);
	const [errorMessage, setErrorMessage] = useState("");
	const [isUserLogged, setIsUserLogged] = useState(false);

	

	const logUserIn = (loggedOwnerId: string) => {
		setLocalStorageItem("loggedUser", loggedOwnerId);
		props.fetchLoggedOwner(loggedOwnerId);
	};

	const signInViaFirebase = (...emailAndPassword: Array<string>) => {
		const [email, password] = emailAndPassword;

		auth.signInWithEmailAndPassword(email, password)
			.then((res: firebase.auth.UserCredential | null) => {
				if (!res) {
					throw new Error("Server-side error.");
				};
				const loggedOwnerId: string = (res as any).user.uid;

				logUserIn(loggedOwnerId);
				setIsUserLogged(true);
			})
			.catch((error) => {
				setErrorMessage(error.message);
				console.log("Error: ", error.message);
			});
	};

	const processFormData = (EO: React.FormEvent<HTMLFormElement>) => {
		EO.preventDefault();

		const emailNode: HTMLInputElement | null = emailRef.current;
		const passwordNode: HTMLInputElement | null = passwordRef.current;

		if (emailNode && passwordNode) {
			const email: string = emailNode.value;
			const password: string = passwordNode.value;

			setErrorMessage("");
	
			signInViaFirebase(email, password);
		} else {
			setErrorMessage("Passwords don't match");
		}
	};
	const openSignInWithGooglePopup = async () => {
		const provider = new firebase.auth.GoogleAuthProvider();
		provider.addScope('profile');
		provider.addScope('email');

		firebase.auth().signInWithPopup(provider).then((result) => {
		// This gives you a Google Access Token.
		// var token = result.credential.accessToken;
		const {user, additionalUserInfo} = result;
		const userProfile = additionalUserInfo?.profile as any;
		
		if (additionalUserInfo?.isNewUser) {
			const ownerPayload: Owner = {
				id: user?.uid as string,
				key: "",
				name: user?.displayName as string,
				email: userProfile.email,
				location: "",
				avatar: user?.photoURL as string
			};
			props.addOwner(ownerPayload);
		}
		return user;
		

		})
		.then((user) => {
			logUserIn(user?.uid as string);
			setIsUserLogged(true);
		})
		.catch(err => console.log(err))
	};

	if (isUserLogged) {
		return <Redirect to="/" />
	}
	return (
		<div className="Login-Component-Container">
				<h1 className="Login-Component-Container__Title">Sign In</h1>
				<form className="Login-Component-Form" onSubmit={processFormData}>
						<div className="Login-Input-Container">
								<label className="Login-Input-Container__Label">E-mail : <br/>
										<input 
											className="Login-Input-Container__Input" 
											type="email" 
											ref={emailRef} 
											placeholder="some@mail.com" 
											required 
										/>
								</label>
								<label className="Login-Input-Container__Label">Password : <br/>
										<input 
											className="Login-Input-Container__Input" 
											type="password" 
											ref={passwordRef}  
											placeholder="More than 6 symbols" 
											required 
										/>
								</label>
						</div>
						{
							errorMessage &&
							<div className="Message-Block">
								{errorMessage}
							</div>
						}
						

						<div className="Login-Buttons-Container">
								<input 
									type="submit" 
									value="Отправить" 
									className='Login-Component-Form__Submit' 
								/>
								<NavLink 
									to="/"
									type="button" 
									className='Login-Component-Form__Cancel'
								>Отмена</NavLink>
						</div>
						
				</form>
				<br/>
				<span className="Login-Component-Container__Registartion-Row">
						Don't have an account yet?  
						<NavLink to="/signup" className="Login-Component-Container__Registartion-Link">Sign Up</NavLink>
				</span>
				<button onClick={openSignInWithGooglePopup}>Sign in with Google</button>
		</div>
	);
};



const mapStateToProps = () => {
	return {};
};

const mapDispatchToProps = (
		dispatch: ThunkDispatch<{}, {}, Action>
	) => (
	bindActionCreators(
	{
		fetchLoggedOwner,
		addOwner
	}, 
		dispatch
	)
);

export default connect(mapStateToProps, mapDispatchToProps)(LoginPopup);