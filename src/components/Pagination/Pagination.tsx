import React, {useState, useEffect, useRef} from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import './Pagination.scss';
import { useDispatch, useSelector } from 'react-redux';
import { State, Thing } from '../../types/types';
import { fetchVisibleThings, setVisibleThings, setVisibleFilteredThings } from '../../actions/thingsActions';

function Pagination() {
  const dispatch = useDispatch();
  const things = useSelector((state: State) => state.things);
  const filteredThings = useSelector((state: State) => state.filteredThings);
  const visibleFilteredThings = useSelector((state: State) => state.visibleFilteredThings);
  const searchBarState = useSelector((state: State) => state.searchBarState);
  const isSearchBarActive: boolean = Boolean(searchBarState.inputValue) || searchBarState.isChecked;

  const thingsAmountOnPage = 5;
  const [numOfPages, setNumOfPages] = useState(1);
  const [page, setPage] = useState(1);

  const getArrayOfOrderedNumbers = (amount: number) => {
    let result: number[] = [];
    
    for (let i = 1; i <= amount; i++) {
      result.push(i);
    }
    
    return result;
  };

  const openPage = (page: number) => {
    setPage(page);
    if (isSearchBarActive) {
      setNumOfPages(Math.ceil((filteredThings as Thing[]).length / thingsAmountOnPage));
      dispatch(setVisibleFilteredThings(
        visibleFilteredThings,
        thingsAmountOnPage * (page - 1),
        thingsAmountOnPage
      ));
    } else {
      setNumOfPages(Math.ceil(things.length / thingsAmountOnPage));
      dispatch(fetchVisibleThings(
        things as Thing[],
        thingsAmountOnPage * (page - 1),
        thingsAmountOnPage
      ));
    }
    
    
    
    
  }

  useEffect(() => {
    if (isSearchBarActive) {
      setNumOfPages(Math.ceil((filteredThings as Thing[]).length / thingsAmountOnPage));
    } else {
      setNumOfPages(Math.ceil(things.length / thingsAmountOnPage));
    }
    openPage(1);
  }, [things.length, filteredThings.length]);

  return <div className="container">
    {
      numOfPages 
      ?
      <div>
        {
          getArrayOfOrderedNumbers(numOfPages)
          .map((numberOfPage: number) => {
            return <button key={numberOfPage} onClick={() => openPage(numberOfPage)}>{numberOfPage}</button>
          })
        }
      </div>
      :
      <div>Loading...</div>
    }
  </div>

}


export default Pagination;