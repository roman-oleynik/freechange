import React, {useState, useEffect, Dispatch} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {Redirect, NavLink} from 'react-router-dom';
import { State, Owner, Thing, Action } from '../../types/types';
import ThingItem from '../ThingItem/ThingItem';
import EditButton from '../ThingItem/EditButton/EditButton';
import DeleteButton from '../ThingItem/DeleteButton/DeleteButton';
import { deleteThing, fetchThings } from '../../actions/thingsActions';
import EditDeleteButtons from '../ThingItem/EditDeleteButtons/EditDeleteButtons';
import ConfirmPopup from '../Popups/ConfirmPopup/ConfirmPopup';
import "./UserProfile.scss";
import { getLocalStorageItem } from '../../modules/localStorageAPI';


export interface IMessageBox {
	title: string
	message: string
	close: () => void
}

export interface IConfirmPopup extends IMessageBox {
	isBusy?: boolean
	onYesPressed: () => void
};


export interface IButton {
	value: string
	cssClass: string
	callback: () => void
	linkTo: string
};

export function BusyIndicator() {
	return <div className="Busy-Indicator">Loading...</div>
}

export function MessageBox(props: IMessageBox) {
	return <div className="Message-Box Message-Box_Confirm">
		<h4 className="Message-Box__Title">{props.title}</h4>
		<p className="Message-Box__Text">{props.message}</p>
		<button
			className="Message-Box__Button Message-Box__Button_Ok"
			onClick={props.close}       
		>Ок</button>
	</div>
};





export function Button(props: IButton) {
	return <NavLink
		to={props.linkTo}
		className={`${props.cssClass}`}
		onClick={props.callback}       
	>{props.value}</NavLink>
}





function DeleteThingDialog(props: IConfirmPopup) {
	return <ConfirmPopup
		title={props.title}
		message={props.message}
		isBusy={props.isBusy}
		onYesPressed={props.onYesPressed}
		close={props.close}
	/>
};


function UserProfile() {
	const dispatch = useDispatch();

	const localStorageLoggedUserData = getLocalStorageItem("loggedUser");
    const localStorageDoesntHaveDataAboutLoggedUser = !localStorageLoggedUserData;

	const loggedOwner = useSelector((state: State) => state.loggedOwner);
	const things = useSelector((state: State) => state.things);

	const [dialog, setDialog] = useState(<></>);
	const [isBusy, setIsBusy] = useState(false);
	
	useEffect(() => {
		dispatch(fetchThings());
	}, [loggedOwner])


	const closeDialog = () => {
		setDialog(<></>);
	};

	const setBusyIndicatorVisible = () => {
		setIsBusy(true);
	};
	const setBusyIndicatorInvisible = () => {
		setIsBusy(false);
	};

	const updateThingsAfterDeleting = () => {
		dispatch(fetchThings());
	};

	async function handleDeleteThingConfirmation(thingKey: string | undefined) {
		if (thingKey) {
			closeDialog();
			// setIsBusy(true);
			await dispatch(deleteThing(thingKey));
			await updateThingsAfterDeleting();
			// setIsBusy(false);
		} else {
			throw new Error("The data- attribute of Delete button wasn't got.")
		}
	};

	
	
	// function openAddThingDialog() {
	// 	setDialog(<AddThingPopup
	// 		title="Добавить вещь"
	// 		setBusyIndicatorVisible={setBusyIndicatorVisible}
	// 		setBusyIndicatorInvisible={setBusyIndicatorInvisible}
	// 		closeDialog={closeDialog}	
	// 	/>);
	// };
	function openDeleteThingConfirmationDialog(EO: React.MouseEvent<HTMLButtonElement>) {
		const thingKey = (EO.target as HTMLButtonElement).dataset.key;

		setDialog(<DeleteThingDialog
			title="Удаление вещи"
			message="Вы действительно хотите удалить эту вещь?"
			onYesPressed={() => handleDeleteThingConfirmation(thingKey)}
			close={closeDialog}
		/>);
	};
	function parseStringifiedObject<T>(stringifiedObject: string): T {
		const parsedObject = JSON.parse(stringifiedObject);
		
		return parsedObject;
	}
	// function openEditThingDialog(EO: React.MouseEvent<HTMLButtonElement>) {
	// 	const stringifiedThingData = (EO.target as HTMLButtonElement).dataset.thing;
	// 	const thingObject = parseStringifiedObject<Thing>(stringifiedThingData as string);

	// 	setDialog(<EditThingDialog 
	// 		title="Изменить параметры вещи"
	// 		thing={thingObject}
	// 		setBusyIndicatorVisible={setBusyIndicatorVisible}
	// 		setBusyIndicatorInvisible={setBusyIndicatorInvisible}
	// 		closeDialog={closeDialog}
	// 	/>)
	// };

	if (localStorageDoesntHaveDataAboutLoggedUser) {
        return <Redirect to="/" />
    }
	
	if (!loggedOwner) {
		return <div>Loading...</div>
	}
	return <div className="User-Profile">
		<h1>{loggedOwner.name}</h1>
		{
			loggedOwner.avatar
			?
			<img src={loggedOwner.avatar} style={{width: "300px"}} alt="Avatar"/>
			:
			<div>No avatar</div>
		}
		<p>{loggedOwner.email}</p>
		<p>{loggedOwner.location}</p>
		<NavLink to="/settings">Edit Profile</NavLink>
		<div style={{display: "flex", flexWrap: "wrap"}}>
			{
				things.reduce((filteredThings: React.ReactElement[], thing: Thing) => {
					const loggedOwnerId: string = (loggedOwner as Owner).id;

					if (thing.rel_Owner === loggedOwnerId) {
						filteredThings.push(
							<ThingItem 
								key={thing.id}
								data={thing}
								buttons={
									<EditDeleteButtons>
										<EditButton 
											thing={thing} 
										/>
										<DeleteButton 
											firebaseKey={thing.key} 
											openDeleteThingConfirmationDialog={openDeleteThingConfirmationDialog} 
										/>
									</EditDeleteButtons>
								}
							/>
						);
					}
					return filteredThings;
				}, [])
			}
		</div>
		<Button 
			linkTo="/profile/addThing"
			value="Добавить вещь"
			cssClass="Add-Thing-Button"
			callback={() => {}}
		/>
		
		{
			dialog
		}
	</div>
};

export default UserProfile;
