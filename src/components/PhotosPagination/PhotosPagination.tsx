import React from 'react';
import { Photo } from '../../types/types';
import './PhotosPagination.scss';

type OwnProps = {
    photos: Photo[]
    page: number
}

function PhotosPagination(props: OwnProps) {
    return <div className="Pagination">
        {
            props.photos.map((photo: Photo, index: number) => {
                return <span
                    key={photo.id}
                    className={`Pagination__Page ${index === props.page ? "Selected-Page" : null}`}
                ></span>
            })
        }
    </div>
}

export default PhotosPagination;