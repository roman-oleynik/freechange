import React, { FormEvent, useRef, useState } from 'react';
import {connect, DispatchProp} from 'react-redux';
import {State, Thing, SearchBarState, Action} from '../../types/types';
import { 
    setThingNameInputValue, 
    setIsFreeCheckboxValue, 
    fetchVisibleThings, 
    filterDataOnSearch,
    setFilteredThings,
    discardParametersOfThingsSearching
} from '../../actions/thingsActions';
import './SearchBar.scss';
import { ThunkDispatch } from 'redux-thunk';
import { bindActionCreators } from 'redux';
import { Redirect } from 'react-router-dom';

interface StateProps {
    searchBarConfig: SearchBarState
    filteredThings: Thing[]
    things: Thing[]
};
interface DispatchProps {
    discardParametersOfThingsSearching: () => void
    filterDataOnSearch: (config: SearchBarState) => void
    setIsFreeCheckboxValue: (value: boolean) => void
    setThingNameInputValue: (inputValue: string) => void
    fetchVisibleThings: (things: Thing[], from: number, amount: number) => void
};
interface OwnProps {
    match?: any
};

type Props = StateProps & DispatchProps & OwnProps;


function SearchBar(props: Props) {
    const thingsSearchInput = useRef<HTMLInputElement>(null);
    const thingsSearchCheckbox = useRef<HTMLInputElement>(null);

    async function handleFormSubmition(EO: React.FormEvent<HTMLFormElement>) {
        EO.preventDefault();

        const payload: SearchBarState = {
            inputValue: thingsSearchInput.current?.value as string,
            isChecked: thingsSearchCheckbox.current?.checked as boolean
        };

        await props.setThingNameInputValue(payload.inputValue);
        await props.setIsFreeCheckboxValue(payload.isChecked);
        props.filterDataOnSearch(payload);
        
    };
    function clearInputs() {
        const searchInputDOMNode = thingsSearchInput.current;
        const checkboxDOMNode = thingsSearchCheckbox.current;
        if (searchInputDOMNode) {
            searchInputDOMNode.value = "";
        }
        if (checkboxDOMNode) {
            checkboxDOMNode.checked = false;
        }
    };
    async function discardSearchResults(EO: React.MouseEvent<HTMLInputElement>) {
        EO.preventDefault();

        const allThings: SearchBarState = {
            inputValue: "",
            isChecked: false
        };

        clearInputs();
        await props.setThingNameInputValue("");
        await props.setIsFreeCheckboxValue(false);
        props.discardParametersOfThingsSearching();
    };

    
    return <div className="Search-Bar">
        <form onSubmit={handleFormSubmition} className="Search-Form">
            <input 
                className="Search-Form__Thing-Input"
                type="text"
                ref={thingsSearchInput}
                defaultValue={props.searchBarConfig.inputValue}
                placeholder="Введите название вещи..."
                required
            />
            <div className="Search-Form__Checkbox-Submit-Row">
                <label className="Search-Form__Is-Free-Label" htmlFor="isFreeCheckbox">For free
                    <input 
                        id="isFreeCheckbox"
                        className="Search-Form__Is-Free-Checkbox"
                        type="checkbox"
                        ref={thingsSearchCheckbox}
                        defaultChecked={props.searchBarConfig.isChecked}
                    />
                </label>
                <div className="Search-Form__Submit-Discard-Buttons">
                    <input 
                        type="button"
                        className="Search-Form__Discard-Button"
                        value="Discard"
                        onClick={discardSearchResults}
                    />
                    <input 
                        className="Search-Form__Submit-Button"
                        type="submit"
                        value="Find things"
                        // disabled={!isFormFilled}
                    />
                </div>
                
            </div>
        </form>
    </div>
}



const mapStateToProps = (state: State): StateProps => ({
    searchBarConfig: state.searchBarState,
    filteredThings: state.filteredThings,
    things: state.things
});

const mapDispatchToProps = (dispatch: ThunkDispatch<{}, {}, Action>) => (
    bindActionCreators(
    {
        setIsFreeCheckboxValue,
        setThingNameInputValue,
        fetchVisibleThings,
        filterDataOnSearch,
        discardParametersOfThingsSearching
    },
        dispatch
    )
);

export default connect(mapStateToProps, mapDispatchToProps)(SearchBar);