import React, {useState, useEffect} from 'react';
import App from '../App/App';
import { fetchLoggedOwner } from '../../actions/ownersActions';

function OnlineCheckerHOC() {
  const [online, setOnline] = useState(navigator.onLine);
  const [isOfflineToOnline, setIsOfflineToOnline] = useState(false);
  
  // type the online/offline events
  function changeOnlineState(EO: any) {
    if (navigator.onLine) {
        setOnline(true);
        setIsOfflineToOnline(true);
    } else {
        setOnline(false);
        setIsOfflineToOnline(false);
    }
  };
  useEffect(() => {
    window.addEventListener("online", changeOnlineState);
    window.addEventListener("offline", changeOnlineState);
  });
  
  return <App isOnline={online} isOfflineToOnline={isOfflineToOnline}  />
  
};

export default OnlineCheckerHOC;