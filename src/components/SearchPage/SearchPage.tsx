import React, {useState, useEffect, useRef} from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import SearchBar from '../SearchBar/SearchBar';
// import './About.scss';

interface IProps {
  match: any
}

function SearchPage(props: IProps) {
  const a = props.match.params; 
  console.log(a);
  return <div>
    <SearchBar />
    <h1>Search results for: {props.match.params.inputValue}</h1>
  </div>
  
}


export default SearchPage;