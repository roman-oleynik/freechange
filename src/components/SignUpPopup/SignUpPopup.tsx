import React, {useState, useRef} from 'react';
import {auth} from '../../firebase/firebaseConfig';
import {database} from '../../firebase/firebaseConfig';
import {addOwner} from '../../actions/ownersActions';
import { connect } from 'react-redux';
import { State, Owner, Action } from '../../types/types';
import {Dispatch, bindActionCreators} from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import './SignUpPopup.scss';
import { NavLink, Redirect } from 'react-router-dom';


interface StateProps {};

interface DispatchProps {
    addOwner: (newOwner: Owner) => void
};

interface OwnProps {};

type Props = StateProps & DispatchProps & OwnProps;

function SignUpPopup(props: Props) {
    const [isNewUserCreated, setIsNewUserCreated] = useState(false);

    const emailRef = useRef<HTMLInputElement>(null);
    const passwordRef = useRef<HTMLInputElement>(null);
    const confirmPasswordRef = useRef<HTMLInputElement>(null);
    const nameRef = useRef<HTMLInputElement>(null);
    const locationRef = useRef<HTMLInputElement>(null);

    const createNewOwner = (...newOwnerProperties: string[]) => {
        const [email, password, password2, name, location] = newOwnerProperties;

        if (password === password2) {
            auth.createUserWithEmailAndPassword(email, password)
            .then((res: any) => {
                if (res !== null) {
                    const newOwner: Owner = {
                        id: res.user.uid,
                        key: "",
                        name,
                        email,
                        location,
                        avatar: ""
                    } 
                    props.addOwner(newOwner);
                    setIsNewUserCreated(true);
                } else {
                    console.log("Error while creating a new user")
                }
            })
            .catch(function(error) {
              // Handle Errors here.
              var errorCode = error.code;
              var errorMessage = error.message;
              console.log(errorCode, errorMessage);
              // ...
            });
        } else {
            alert("Password and it's confirmation must match.")
        }
    };

    const processFormData = (EO: React.FormEvent<HTMLFormElement>): void => {
        EO.preventDefault();

        const emailNode: HTMLInputElement | null = emailRef.current;
        const passwordNode: HTMLInputElement | null = passwordRef.current;
        const confirmPasswordNode: HTMLInputElement | null = confirmPasswordRef.current;
        const nameNode: HTMLInputElement | null = nameRef.current;
        const locationNode: HTMLInputElement | null = locationRef.current;

        if (
            emailNode &&
            passwordNode &&
            confirmPasswordNode &&
            nameNode &&
            locationNode
        ) {
            
            const email = emailNode.value;
            const password = passwordNode.value;
            const password2 = confirmPasswordNode.value;
            const name = nameNode.value;
            const location = locationNode.value;

            createNewOwner(email, password, password2, name, location);
        }

        
    };

    if (isNewUserCreated) {
        return <Redirect to="/login" />
    }

    return (
        <div className="Registration-Component-Container">
            <h1 className="Registration-Component-Container__Title">Registration</h1>
            <form className="Registration-Component-Form" onSubmit={processFormData}>
                <div className="Registration-Input-Container">
                    <label className="Registration-Input-Container__Label">E-mail : <br />
                        <input 
                            className="Registration-Input-Container__Input" 
                            ref={emailRef} 
                            type="email" 
                            placeholder="E-mail" 
                            required 
                        />
                    </label>
                    <label className="Registration-Input-Container__Label">Password : <br />
                        <input 
                            className="Registration-Input-Container__Input" 
                            ref={passwordRef} 
                            type="password" 
                            placeholder="More than 6 symbols" 
                            required 
                        />
                    </label>
                    <label className="Registration-Input-Container__Label">Confirm Password : <br />
                        <input 
                            className="Registration-Input-Container__Input" 
                            ref={confirmPasswordRef} 
                            type="password" 
                            required 
                        />
                    </label>
                    <label className="Registration-Input-Container__Label">Name : <br />
                        <input 
                            className="Registration-Input-Container__Input" 
                            ref={nameRef} 
                            type="text" 
                            required 
                        />
                    </label>
                    <label className="Registration-Input-Container__Label">Location : <br />
                        <input 
                            className="Registration-Input-Container__Input" 
                            ref={locationRef} 
                            type="text" 
                        />
                    </label>
                </div>

                <div className="Registration-Buttons-Container">
                    <input 
                        type="submit" 
                        value="Submit" 
                        className='Registration-Component-Form__Submit' 
                    />
                    <NavLink 
                        to="/"
                        className='Registration-Component-Form__Cancel'     
                    >Отмена</NavLink>
                </div>
            </form>
            <br/>
            <span className="Registration-Component-Container__Login-Row">
                Already have an account?  
                <NavLink 
                    to="/login"
                    className="Registration-Component-Container__Login-Link"    
                >Log In</NavLink>
            </span>
        </div>
    );
};

const mapStateToProps = () => {
    return {};
};

const mapDispatchToProps = (dispatch: ThunkDispatch<{}, {}, Action>) => (
    bindActionCreators(
    {
        addOwner
    }, 
        dispatch
    )
);       

export default connect(mapStateToProps, mapDispatchToProps)(SignUpPopup);