import React, {useState, useEffect, useRef} from 'react';
import { connect, DispatchProp, useDispatch } from 'react-redux';
import { Photo, Thing, State, Action } from '../../types/types';
import {fetchPhotos, setPhotos} from '../../actions/photosActions';
import PhotosPagination from '../PhotosPagination/PhotosPagination';
import './PhotosCarousel.scss';
import { bindActionCreators } from 'redux';
import { ThunkDispatch } from 'redux-thunk';



interface StateProps {
    thing: Thing
    photos: Photo[]
};
interface DispatchProps {
    fetchPhotos: (thingId: string) => void
    setPhotos: (photos: Photo[]) => Action
};
interface OwnProps {};

type Props = StateProps & DispatchProps & OwnProps;


function PhotosCarousel(props: Props) {
    const defaultImageSrc = "https://flevix.com/wp-content/uploads/2019/07/Camera-Preloader.gif";
    
    const [isImageLoaded, setIsImageLoaded] = useState(false);
    const [page, setPage] = useState(0);
    const [isPrevButtonDisabled, setIsPrevButtonDisabled] = useState(true);
    const [isNextButtonDisabled, setIsNextButtonDisabled] = useState(false);

    const imageRef = useRef<HTMLImageElement>(null);

    const imageStyle_Visible = {
        display: "block"
    };
    const imageStyle_Invisible = {
        display: "none"
    };

    const waitForImageLoading = (ref: React.RefObject<HTMLImageElement>): void => {
        if (ref.current) {
            ref.current.onload = () => {
                setIsImageLoaded(true);
            }
        }
    }
    
    const refreshPhotos = async (thingId: string, imageRef: React.RefObject<HTMLImageElement>) => {
        props.setPhotos([]);
        await props.fetchPhotos(thingId);
        waitForImageLoading(imageRef);
    }

    useEffect(() => {
        waitForImageLoading(imageRef);
    }, [imageRef]);

    const thingId = props.thing.id;

    useEffect(() => {
        refreshPhotos(thingId, imageRef);
    }, [thingId]);

    

    function increasePageNumber() {
        if (page >= props.photos.length-2) {
            setIsNextButtonDisabled(true);
        } else {
            setIsNextButtonDisabled(false);
        }
        setPage(page+1);
        setIsPrevButtonDisabled(false);
    }
    function decreasePageNumber() {
        if (page <= 1) {
            setIsPrevButtonDisabled(true);
        } else {
            setIsPrevButtonDisabled(false);
        }
        setPage(page-1);
        setIsNextButtonDisabled(false);
    }


    function setPageNumber(EO: any) {
        setIsImageLoaded(false);
        const setPageNumberMode:string = EO.target.dataset.changePageNumber;
        if (setPageNumberMode === "decrease") {
            decreasePageNumber();
        } else if (setPageNumberMode === "increase") {
            increasePageNumber();
        }
        waitForImageLoading(imageRef);
    }

    return <div className="Carousel">
        <div className="Carousel__Screen">
            {
                props.photos.length
                ?
                <div className="Carousel__Image-Container">
                    <img
                        style={isImageLoaded ? imageStyle_Invisible : imageStyle_Visible} 
                        className="Carousel__Image_Preloader" 
                        src={defaultImageSrc} 
                        alt="Preloader"
                    />
                    <img
                        style={isImageLoaded ? imageStyle_Visible : imageStyle_Invisible} 
                        ref={imageRef}
                        className="Carousel__Image_Image" 
                        src={props.photos[page].source} 
                        alt="Thing" 
                    />
                </div>
                :
                <div className="Carousel__Empty-Space">No photos</div>
            }
            {
                props.photos && props.photos.length > 1 &&
                <>
                <button data-change-page-number="decrease" disabled={isPrevButtonDisabled} onClick={setPageNumber} className="Carousel__Prev-Button">Prev</button>
                <button data-change-page-number="increase" disabled={isNextButtonDisabled} onClick={setPageNumber} className="Carousel__Next-Button">Next</button>
                </>
            }
            
        </div>
        <PhotosPagination photos={props.photos} page={page} />
    </div>
}

const mapStateToProps = (state: State): StateProps => (
    {
        photos: state.thingPhotos,
        thing: state.thing
    }
);

const mapDispatchToProps = (dispatch: ThunkDispatch<{}, {}, Action>) => (
    bindActionCreators({
        fetchPhotos,
        setPhotos
    },
        dispatch
    )
);

export default connect(mapStateToProps, mapDispatchToProps)(PhotosCarousel);