import React, {useState} from 'react';
import {connect, DispatchProp} from 'react-redux';
import {fetchThings} from '../../actions/thingsActions';
import {Thing, State, Action, SearchBarState} from '../../types/types';
import ThingItem from '../ThingItem/ThingItem';
import SearchBar from '../SearchBar/SearchBar';
import './Main.scss';
import { ThunkDispatch } from 'redux-thunk';
import { bindActionCreators } from 'redux';
import Pagination from '../Pagination/Pagination';

interface StateProps {
    things: Thing[],
    visibleThings: Thing[],
	filteredThings: Thing[],
	visibleFilteredThings: Thing[],
	searchBarState: SearchBarState
};
interface DispatchProps {
	fetchThings: () => void
	fetchFilteredBySearchData: (config: SearchBarState) => void
};
interface OwnProps {};

type Props = StateProps & DispatchProps & OwnProps;

interface IState {
	isContentLoaded: boolean
}

class Main extends React.Component<Props, IState> {
  	readonly state: IState = {
	  isContentLoaded: false
	};
	public isSearchBarActive: boolean = Boolean(this.props.searchBarState.inputValue) 
	|| this.props.searchBarState.isChecked;

	componentDidMount = async () => {
		await this.props.fetchThings();
		this.setState({isContentLoaded: true});
	};
	render() {
		if (!this.state.isContentLoaded) {
			return <h1>Loading...</h1>
		} else {
			return <div className="App">
				<SearchBar />
				{
					(Boolean(this.props.searchBarState.inputValue) || this.props.searchBarState.isChecked)
					?
					<>
						<Pagination />
						<div className="Things-Container">
						{
							this.props.visibleFilteredThings.length
							?
							this.props.visibleFilteredThings.map((thing:Thing) => {
								return <ThingItem key={thing.id} data={thing} />
							})
							:
							<div>Things' list is empty</div>
						}
						</div>
					</>
					:
					<>
						<Pagination />
						<div className="Things-Container">
						{
							this.props.visibleThings.length
							?
							this.props.visibleThings.map((thing:Thing) => {
								return <ThingItem key={thing.id} data={thing} />
							})
							:
							<div>Things' list is empty</div>
						}
						</div>
					</>
				}
				
			</div>
		}	
	};
};

const mapStateToProps = (state: State): StateProps => ({
  things: state.things,
  visibleThings: state.visibleThings,
  filteredThings: state.filteredThings,
  visibleFilteredThings: state.visibleFilteredThings,
  searchBarState: state.searchBarState
});

const mapDispatchToProps = (dispatch: ThunkDispatch<{}, {}, Action>) => (
	bindActionCreators(
	{
		fetchThings,
	},
		dispatch
	)
);

export default connect(mapStateToProps, mapDispatchToProps)(Main);