import React, {useState, useRef, useEffect, ReactElement} from 'react';
import { generateId } from '../../../modules/generateId';
import { connect, useDispatch, useSelector } from 'react-redux';
import { State, Owner, Thing, Photo } from '../../../types/types';
import { addThing, fetchThings } from '../../../actions/thingsActions';
import {Dialog, IDialog} from '../Dialog/Dialog';
import { InputElement } from '../EditThingDialog/EditThingDialog';
import './AddThingPopup.scss';
import { Redirect } from 'react-router-dom';


export interface IAddThingDialogProps {
    title: string
    setBusyIndicatorVisible: () => void
    setBusyIndicatorInvisible: () => void
    closeDialog: () => void
};

function AddThingPopup(props: IAddThingDialogProps) {
    const DEFAULT_AVATAR_PATH = "http://cdn.onlinewebfonts.com/svg/img_343322.png";
    const dispatch = useDispatch();
    const loggedOwner = useSelector((state: State) => state.loggedOwner);

    const [tempPhotos, setTempPhotos] = useState<any>([]);
    const [isThingCreated, setIsThingCreated] = useState(false);


    const nameRef = useRef<HTMLInputElement>(null);
    const nameNode: HTMLInputElement | null = nameRef.current;

    const priceRef = useRef<HTMLInputElement>(null);
    const priceNode: HTMLInputElement | null = priceRef.current;

    const currencyRef = useRef<HTMLSelectElement>(null);
    const currencyNode: HTMLSelectElement | null = currencyRef.current;

    const descriptionRef = useRef<HTMLTextAreaElement>(null);
    const descriptionNode: HTMLTextAreaElement | null = descriptionRef.current;

    const [payload, setPayload] = useState({
        id: generateId(),
        name: "",
        description: "",
        avatar: "https://flevix.com/wp-content/uploads/2019/07/Camera-Preloader.gif",
        rel_Owner: "",
        price: Number(priceNode?.value),
        currency: "BYN",
        key: ""
    });

    useEffect(() => {
        if (loggedOwner?.id) {
            setPayload({...payload, rel_Owner: (loggedOwner as Owner).id})
        }
    }, [loggedOwner])

    const processFormData = async (EO: React.FormEvent<HTMLFormElement>) => {
        EO.preventDefault();

        setIsThingCreated(true);
        dispatch(addThing(payload, tempPhotos));
    };

    const editPayload = (
        prop: string, 
        propNode: InputElement | null
    ) => {
        setPayload({...payload, [prop]: propNode ? propNode.value : ""})
    };

    const handleAvatarInputChange = (EO: React.ChangeEvent<HTMLInputElement>) => {
        let files = EO.target.files && EO.target.files;
        var loadedPhotos: Photo[] = [] as Photo[];

        if (files) {
            [].forEach.call(files, readAndPreview);
        }

        function readAndPreview(file: File) {
            const hasPictureFormat = /\.(jpe?g|png|gif)$/i.test(file.name);

            if ( hasPictureFormat ) {
                var reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = function() {
                    loadedPhotos.push({
                        id: generateId(),
                        key: "",
                        source: reader.result as string,
                        rel_Thing: ""
                    })
                    setTempPhotos([...tempPhotos, ...loadedPhotos]);
                };
                reader.onerror = function() {
                    console.log(reader.error);
                };
            }
        }
    };

    function deleteTempPhoto(EO: any) {
        EO.preventDefault();

        const delPhotoId = EO.target.dataset.id;
        const filteredTempPhotos = tempPhotos.filter((photo: Photo) => photo.id !== delPhotoId)
        setTempPhotos(filteredTempPhotos);
    };

    if (isThingCreated) {
        return <Redirect to="/profile" />
    }
    return <Dialog
        title={props.title}
        setBusyIndicatorVisible={props.setBusyIndicatorVisible}
        setBusyIndicatorInvisible={props.setBusyIndicatorInvisible}
        onSubmit={processFormData}
    >
        
        <div className="Add-Thing-Form__Photos-Inputs-Container">
            <div className="Add-Thing-Form__Photos-Container">
                <label className="Add-Photos-Button">
                    +
                    <input 
                        type="file"
                        multiple
                        className="Add-Photos-Button__Input"
                        onChange={handleAvatarInputChange}
                    />
                </label>
                {
                    tempPhotos.length > 0
                    &&
                    tempPhotos.map((tempPhoto: Photo) => {
                        return <div className="Photos-Container" key={tempPhoto.id}>
                            <img src={tempPhoto.source} className="Photo" alt="Avatar"/>
                            <button
                                data-id={tempPhoto.id} 
                                onClick={deleteTempPhoto}
                                className="Delete-Photo-Button"
                            >x</button>
                        </div>
                    })
                }
            </div>
            <div className="Add-Thing-Form__Inputs-Container">
                <label className="Add-Thing-Form__Input-Label">Name : <br />
                    <input 
                        className="Add-Thing-Form__Input" 
                        onChange={() => editPayload("name", nameNode)}
                        ref={nameRef} 
                        type="text" 
                        required
                    />
                </label>
                <label className="Add-Thing-Form__Input-Label">Price : <br />
                    <input 
                        className="Add-Thing-Form__Input" 
                        onChange={() => editPayload("price", priceNode)}
                        ref={priceRef} 
                        type="number" 
                        required
                    />
                </label>
                <label className="Add-Thing-Form__Input-Label">Currency : <br />
                    <select 
                        className="Add-Thing-Form__Input" 
                        onChange={() => editPayload("currency", currencyNode)}
                        ref={currencyRef} 
                        placeholder="Currency" 
                        required
                    >
                        <option value="BYN">BYN</option>
                        <option value="USD">USD</option>
                        <option value="RUB">RUB</option>
                        <option value="EUR">EUR</option>
                    </select>
                </label>
                <label className="Add-Thing-Form__Input-Label">Description :
                    <textarea 
                        rows={4}
                        className="Add-Thing-Form__Input" 
                        onChange={() => editPayload("description", descriptionNode)}
                        ref={descriptionRef} 
                        placeholder="Description..." 
                    ></textarea>
                </label>
            </div>
        </div> 
    </Dialog>
};


export default AddThingPopup;
