import React from 'react';
import { Thing } from '../../../types/types';
import {Button} from '../../UserProfile/UserProfile';
import './Dialog.scss';

export interface IDialog {
	title: string
	children?: React.ReactElement
	thing?: Thing
	setBusyIndicatorVisible: () => void
	setBusyIndicatorInvisible: () => void
	onSubmit: (EO: React.FormEvent<HTMLFormElement>) => void
};

export function Dialog(props: IDialog) {
	return <div className="Background">
		<div className="Dialog">
			<h4 className="Dialog__Title">{props.title}</h4>
			<div className="Dialog__Body">
				<form 
					onSubmit={props.onSubmit} 
					className="Edit-Thing-Form"
				>
					{props.children}
					<div className="Popup-Action-Buttons">
						<input 
							type="submit"
							className="Dialog__Button Dialog__Button_Submit"
						/>
						<Button 
							linkTo="/profile"
							value="Отмена"
							cssClass="Dialog__Button Dialog__Button_Cancel"
							callback={() => {}}
						/>
					</div>
				</form>
			</div>
			
		</div>
	</div>;
}