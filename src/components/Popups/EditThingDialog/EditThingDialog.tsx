import React, {useState, useRef, ReactElement, useEffect} from 'react';
import './EditThingDialog.scss';
import { generateId } from '../../../modules/generateId';
import { connect, useDispatch, useSelector } from 'react-redux';
import { State, Owner, Thing, Photo } from '../../../types/types';
import { addThing, fetchThings, updateThing } from '../../../actions/thingsActions';
import {Dialog, IDialog} from '../Dialog/Dialog';
import { database } from '../../../firebase/firebaseConfig';
import { fetchLoggedOwner } from '../../../actions/ownersActions';
import { getLocalStorageItem } from '../../../modules/localStorageAPI';
import { Redirect } from 'react-router-dom';



export interface IEditThingDialogProps {
    title: string
    thing: Thing
    setBusyIndicatorVisible: () => void
    setBusyIndicatorInvisible: () => void
    closeDialog: () => void
    match?: any
};

export type InputElement = HTMLInputElement | HTMLSelectElement | HTMLTextAreaElement;

function EditThingDialog(props: IEditThingDialogProps) {
    const dispatch = useDispatch();
    const loggedOwner = useSelector((state: State) => state.loggedOwner);
    const thingId = props.match.params.thingId;


    const [payload, setPayload] = useState({...props.thing});
    const [tempPhotos, setTempPhotos] = useState<any>([]);
    const [isThingEdited, setIsThingEdited] = useState(false);

    const nameInput = useRef<HTMLInputElement>(null);

    const focusOnInput = (input: React.RefObject<HTMLInputElement>) => {
        input.current?.focus();
    };
    const fetchPhotos = () => {
        fetch("https://freechange.firebaseio.com/photos.json")
            .then(response => response.json())
            .then(data => {
                let matchedPhotos: Photo[] = []; 
                if (data) {
                    for (let key of Object.keys(data)) {
                        const photo = data[key];
                        if (photo.rel_Thing === thingId) {
                            matchedPhotos.push(photo);
                        };
                    }
                }
                
                setTempPhotos(matchedPhotos);
            })
    };
    const fetchEditedThing = async () => {
        fetch("https://freechange.firebaseio.com/things.json")
            .then(response => response.json())
            .then(data => {
                for (let key of Object.keys(data)) {
                    const thing = data[key];
                    if (thing.id === thingId) {
                        setPayload(thing);
                        break;
                    };
                }
            })
    };
    async function loadRequiredForDialogData() {
        await dispatch(fetchLoggedOwner(getLocalStorageItem("loggedUser") as string));
        await fetchEditedThing();
        fetchPhotos();
    }

    useEffect(() => {
        focusOnInput(nameInput);
        loadRequiredForDialogData();
    }, [nameInput]);

    const processFormData = async (EO: React.FormEvent<HTMLFormElement>) => {
        EO.preventDefault();
        setIsThingEdited(true);
        dispatch(updateThing(payload, tempPhotos));
    };



    const editPayload = (EO: React.ChangeEvent<InputElement>) => {
        const prop = EO.target.dataset.field as string;
        setPayload({...payload, [prop]: EO.target.value});
    }

    const handleAvatarInputChange = (EO: React.ChangeEvent<HTMLInputElement>) => {
        let files = EO.target.files && EO.target.files;
        var loadedPhotos: Photo[] = [] as Photo[];

        if (files) {
            [].forEach.call(files, readAndPreview);
        }

        function readAndPreview(file: File) {
            const hasPictureFormat = /\.(jpe?g|png|gif)$/i.test(file.name);

            if ( hasPictureFormat ) {
                var reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = function() {
                    loadedPhotos.push({
                        id: generateId(),
                        key: "",
                        source: reader.result as string,
                        rel_Thing: ""
                    })
                    setTempPhotos([...tempPhotos, ...loadedPhotos]);
                };
                reader.onerror = function() {
                    console.log(reader.error);
                };
            }
        }
    };
    function deleteTempPhoto(EO: any) {
        EO.preventDefault();

        const delPhotoId = EO.target.dataset.id;
        const delPhotoKey = EO.target.dataset.key;

        const filteredTempPhotos = tempPhotos.filter((photo: Photo) => photo.id !== delPhotoId)
        setTempPhotos(filteredTempPhotos);

        if (delPhotoKey) {
            database.ref(`/photos/${delPhotoKey}`).remove();
        }   
    };

    if (isThingEdited) {
        return <Redirect to="/profile" />
    }

    return <Dialog 
		title={props.title}
        thing={props.thing}
        setBusyIndicatorVisible={props.setBusyIndicatorVisible}
        setBusyIndicatorInvisible={props.setBusyIndicatorInvisible}
		onSubmit={processFormData}
	>
        <div className="Edit-Thing-Form__Avatar-Inputs-Container">
        <div className="Edit-Thing-Form__Photos-Container">
                <label className="Add-Photos-Button">
                    +
                    <input 
                        type="file"
                        multiple
                        className="Add-Photos-Button__Input"
                        onChange={handleAvatarInputChange}
                    />
                </label>
                {
                    tempPhotos.length > 0
                    &&
                    tempPhotos.map((tempPhoto: Photo) => {
                        return <div className="Photos-Container" key={tempPhoto.id}>
                            <img src={tempPhoto.source} className="Photo" alt="Avatar"/>
                            <button
                                data-id={tempPhoto.id} 
                                data-key={tempPhoto.key} 
                                onClick={deleteTempPhoto}
                                className="Delete-Photo-Button"
                            >x</button>
                        </div>
                    })
                }
            </div>
            <div className="Edit-Thing-Form__Inputs-Container">
                <label className="Edit-Thing-Form__Input-Label">Name : <br />
                    <input 
                        className="Edit-Thing-Form__Input"
                        onChange={editPayload}
                        defaultValue={payload.name}
                        ref={nameInput}
                        data-field="name"
                        type="text" 
                        placeholder="Type the name of a thing" 
                        required
                    />
                </label>
                <label className="Edit-Thing-Form__Input-Label">Price : <br />
                    <input 
                        className="Edit-Thing-Form__Input" 
                        defaultValue={payload.price}
                        onChange={editPayload}
                        data-field="price"
                        type="number" 
                        placeholder="Type the name of a thing" 
                        required
                    />
                </label>
                <label className="Edit-Thing-Form__Input-Label">Currency : <br />
                    <select 
                        className="Edit-Thing-Form__Input"
                        defaultValue={payload.currency}
                        onChange={editPayload}
                        data-field="currency"
                        placeholder="Currency" 
                        required
                    >
                        <option value="BYN">BYN</option>
                        <option value="USD">USD</option>
                        <option value="RUB">RUB</option>
                        <option value="EUR">EUR</option>
                    </select>
                </label>
                <label className="Edit-Thing-Form__Input-Label">Description :
                    <textarea 
                        rows={4}
                        className="Edit-Thing-Form__Input" 
                        defaultValue={payload.description || ""}
                        onChange={editPayload}
                        data-field="description"
                        placeholder="Description..." 
                    ></textarea>
                </label>
            </div>
        </div>
	</Dialog>
};


export default EditThingDialog;
