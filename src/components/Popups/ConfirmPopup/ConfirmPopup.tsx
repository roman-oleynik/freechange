import React, { useRef, useEffect } from 'react';

import './ConfirmPopup.scss';


export interface IMessageBox {
	title: string
	message: string
	close: () => void
}

export interface IConfirmPopup extends IMessageBox {
	isBusy?: boolean
	onYesPressed: () => void
};

export default function ConfirmPopup(props: IConfirmPopup) {
    const yesButtonRef = useRef<HTMLButtonElement>(null);

    useEffect(() => {
        if (yesButtonRef.current) {
            yesButtonRef.current.focus();
        }
    }, [yesButtonRef]);
    
	return <div className="Background">
        <div className="Message-Box Message-Box_Confirm">
            <h4 className="Message-Box__Title">{props.title}</h4>
            <p className="Message-Box__Text">{props.message}</p>
            <div className="Popup-Action-Buttons">
                <button
                    ref={yesButtonRef}
                    className="Message-Box__Button Message-Box__Button_Yes"
                    onClick={props.onYesPressed}
                >Да</button>
                <button 
                    className="Message-Box__Button Message-Box__Button_Cancel"
                    onClick={props.close}
                >Нет</button>
            </div>
        </div>
    </div>
};