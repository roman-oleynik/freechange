import React, {useState, useEffect} from 'react';
import {connect, DispatchProp, useDispatch, useSelector} from 'react-redux';
import {BrowserRouter, Switch, Route, Link} from 'react-router-dom';
import {ErrorBoundary} from '../../ErrorBoundary';
import Main from '../Main/Main';
import Navbar from '../Navbar/Navbar';
import ThingPage from '../ThingPage/ThingPage';
import OwnerPage from '../OwnerPage/OwnerPage';
import { State, Thing, SearchBarState, Owner, Action } from '../../types/types';
import OfflineMessage from '../OfflineMessage/OfflineMessage';
import BackOnlineMessage from '../BackOnlineMessage/BackOnlineMessage';
import About from '../About/About';
import UserProfile from '../UserProfile/UserProfile';
import { fetchLoggedOwner } from '../../actions/ownersActions';
import { getLocalStorageItem } from '../../modules/localStorageAPI';
import UserSettings from '../UserSettings/UserSettings';
import './App.scss';
import LoginPopup from '../LoginPopup/LoginPopup';
import SignUpPopup from '../SignUpPopup/SignUpPopup';
import AddThingPopup from '../Popups/AddThingPopup/AddThingPopup';
import EditThingDialog from '../Popups/EditThingDialog/EditThingDialog';
import SearchBar from '../SearchBar/SearchBar';
import SearchPage from '../SearchPage/SearchPage';


type Props = {
  isOnline: boolean
  isOfflineToOnline: boolean
};



function App(props: Props) {
  const dispatch = useDispatch();

  const {isOnline, isOfflineToOnline} = props;

  useEffect(() => {
    // test
    const loggedOwnerId: string | null = getLocalStorageItem("loggedUser");    
    if (loggedOwnerId) {
      dispatch(fetchLoggedOwner(loggedOwnerId));
    }
  }, [])

  return (
    <BrowserRouter>
      <Navbar />
      <Switch>
        <Route path="/" exact component={Main} />
        <Route path="/login" component={LoginPopup} />
        <Route path="/signup" component={SignUpPopup} />
        <Route path="/about" component={About} />
        <Route path="/search/:inputValue/:isChecked" component={SearchPage} />
        <Route path="/thing/:thingId" component={ThingPage} />
        <Route path="/owner/:ownerId" component={OwnerPage} />
        <Route path="/profile" exact component={UserProfile} />
        <Route path="/profile/addThing" component={AddThingPopup} />
        <Route path="/profile/editThing/:thingId" component={EditThingDialog} />
        <Route path="/settings" component={UserSettings} />
      </Switch>
      {
        !isOnline &&
        <OfflineMessage />
      }
      {
        isOfflineToOnline &&
        <BackOnlineMessage />
      }
    </BrowserRouter>
  );
  
}



export default App;
