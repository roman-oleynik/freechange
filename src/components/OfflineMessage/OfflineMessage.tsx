import React, {useEffect} from 'react';
import './OfflineMessage.scss';


function OfflineMessage() {
    return (
        <div className="Offline-Message-Toast">
            Нет подключения к интернету
        </div>
    );
    
}

export default OfflineMessage;