import React, { useRef, useEffect, useState } from 'react';
import {Thing} from '../../types/types';
import {NavLink} from 'react-router-dom';
import './ThingItem.scss';

type OwnProps = {
    data: Thing
    buttons?: React.ReactElement<Array<React.ReactElement>>
}

export default function ThingItem(props: OwnProps) {
    const defaultImageSrc = "https://flevix.com/wp-content/uploads/2019/07/Camera-Preloader.gif";
    const [isImageLoaded, setIsImageLoaded] = useState(false);
    const imageRef = useRef<HTMLImageElement>(null);

    const imageStyle_Visible = {
        display: "block"
    };
    const imageStyle_Invisible = {
        display: "none"
    };

    const waitForImageLoading = (ref: React.RefObject<HTMLImageElement>): void => {
        if (ref.current) {
            ref.current.onload = () => {
                setIsImageLoaded(true);
            }
        }
    }
    useEffect(() => {
        waitForImageLoading(imageRef);
    }, [imageRef]);

    const priceLabel = <span className={`Price-Label ${Number(props.data.price) === 0 && "Free"}`}>
        {
            Number(props.data.price) !== 0 ? `${props.data.price} ${props.data.currency}` : "For free"
        }
    </span>;

    return <div className="Thing-Item">
        {
            priceLabel
        }
        {
            props.data.avatar
            ?
            <NavLink to={`/thing/${props.data.id}`}>
                <img 
                    style={isImageLoaded ? imageStyle_Invisible : imageStyle_Visible} 
                    className="Thing-Item__Photo" 
                    src={defaultImageSrc} 
                    alt="Preloader"
                />
                <img 
                    style={isImageLoaded ? imageStyle_Visible : imageStyle_Invisible} 
                    ref={imageRef} 
                    className="Thing-Item__Photo" 
                    src={props.data.avatar} 
                    alt="Thing"
                />
            </NavLink>
            :
            <div className="No-Photo-Fallback-Container">No Photos</div>
        }
        <NavLink to={`/thing/${props.data.id}`} className="Thing-Item__Title">{props.data.name}</NavLink>
        {
            props.data.description
            ?
            <p className="Thing-Item__Description">{props.data.description}</p>
            :
            null
        }
        {
            props.buttons && <>{props.buttons}</>
        }
    </div>
};