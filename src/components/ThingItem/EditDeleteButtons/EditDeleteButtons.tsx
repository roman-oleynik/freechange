import React from 'react';
import './EditDeleteButtons.scss';

type Props = {
    children: Array<React.ReactElement>
}

function EditDeleteButtons(props: Props) {
    return <div className="Edit-Delete-Buttons">
        {props.children}
    </div>
};

export default EditDeleteButtons;