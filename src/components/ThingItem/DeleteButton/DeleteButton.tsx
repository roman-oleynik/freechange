import React from 'react';
import './DeleteButton.scss';

type OwnProps = {
    firebaseKey: string
    openDeleteThingConfirmationDialog: (EO: any) => void
}

function DeleteButton(props: OwnProps) {
    
    return <button 
        data-key={props.firebaseKey}
        className="Thing-Item__Delete-Button"
        onClick={props.openDeleteThingConfirmationDialog}
    >D</button>
};

export default DeleteButton;