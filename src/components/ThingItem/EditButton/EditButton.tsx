import React from 'react';
import './EditButton.scss';
import { Thing } from '../../../types/types';
import { NavLink } from 'react-router-dom';

type OwnProps = {
    thing: Thing
}

function EditButton(props: OwnProps) {
    const stringifiedThing: string = JSON.stringify(props.thing);

    return <NavLink
        to={`/profile/editThing/${props.thing.id}`}
        data-thing={stringifiedThing}
        className="Thing-Item__Edit-Button"
    >E</NavLink>
};

export default EditButton;