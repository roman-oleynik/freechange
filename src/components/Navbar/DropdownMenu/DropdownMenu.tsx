import React, {useEffect, useState} from 'react';
import {NavLink, Redirect} from 'react-router-dom';
import {connect} from 'react-redux';
import './DropdownMenu.scss';
import { State, Owner, Action } from '../../../types/types';
import { setLoggedOwner } from '../../../actions/ownersActions';
import { bindActionCreators } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { removeLocalStorageItem } from '../../../modules/localStorageAPI';
import { auth } from '../../../firebase/firebaseConfig';



interface StateProps {
    loggedOwner: Owner | null
};
interface DispatchProps {
    setLoggedOwner: (loggedOwner: Owner | null) => Action
};
interface OwnProps {}

type Props = StateProps & DispatchProps & OwnProps;



function DropdownMenu(props: Props) {

	const logOut = () => {
		auth.signOut().then(() => {
			removeLocalStorageItem("loggedUser");
			props.setLoggedOwner(null);
		}).catch(err => {
			throw new Error("Error while signing out: ");
		})
	};

	if (!props.loggedOwner) {
		return <Redirect to="/" />;
	} else {
		return (
			<div className="User-Dropdown-Menu">
				<nav>
					<ul className="User-Dropdown-Menu__Navigation-List">
							<NavLink to="/profile" className="User-Dropdown-Menu__Navigation-List-Item">Profile</NavLink>
							<NavLink to="/settings" className="User-Dropdown-Menu__Navigation-List-Item">Settings</NavLink>
							<li className="User-Dropdown-Menu__Navigation-List-Item">
									<button className="User-Dropdown-Menu__Log-Out" onClick={logOut}>Log Out</button>
							</li>
					</ul>
				</nav>
			</div>
		);
	};
};

const mapStateToProps = (state: State): StateProps => ({
    loggedOwner: state.loggedOwner
});

const mapDispatchToProps = (dispatch: ThunkDispatch<{}, {}, Action>) => (
  bindActionCreators(
	{
    setLoggedOwner
	}, 
		dispatch
	)
);

export default connect(mapStateToProps, mapDispatchToProps)(DropdownMenu);