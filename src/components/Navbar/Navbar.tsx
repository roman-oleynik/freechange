import React, {useState} from 'react';
import './Navbar.scss';
import { NavLink } from 'react-router-dom';
import {connect} from 'react-redux';
import {State, Owner} from '../../types/types';
import DropdownMenu from './DropdownMenu/DropdownMenu';
import LoginPopup from '../LoginPopup/LoginPopup';
import SignUpPopup from '../SignUpPopup/SignUpPopup';

interface StateProps {
    loggedOwner: Owner | null
}

function Navbar(props: StateProps) {
    const [isLoginPopupOpened, setIsLoginPopupOpened] = useState(false);
    const [isSignUpPopupOpened, setIsSignUpPopupOpened] = useState(false);
    const [isDropdownOpened, setIsDropdownOpened] = useState(false);

    const openLoginPopup = () => {
        setIsSignUpPopupOpened(false);
        setIsLoginPopupOpened(true);
    };
    const closeLoginPopup = () => {
        setIsLoginPopupOpened(false);
    };
    const openSignUpPopup = () => {
        setIsLoginPopupOpened(false);
        setIsSignUpPopupOpened(true);
    };
    const closeSignUpPopup = () => {
        setIsSignUpPopupOpened(false);
    };

    const toggleDropdown = () => {
        setIsDropdownOpened(!isDropdownOpened);
    }

    return (<>
        <div className="Navbar">
            <h1 className="Navbar__Title">FREECHANGE</h1>
            <nav className="Navbar__Navigation">
                <div className="Navbar__Navigation-List">
                    <NavLink to="/" className="Navbar__Navigation-List-Item Navbar__Navigation-List-Item_Highlighted">Home</NavLink>
                    <NavLink to="/about" className="Navbar__Navigation-List-Item">About</NavLink>
                </div>
            </nav>
            {
                props.loggedOwner
                ?
                <div className="Navbar__User-Menu" onClick={toggleDropdown}>
                    <div 
                        className="Navbar__User-Menu-Avatar-Container"
                        style={{background: `url(${props.loggedOwner.avatar}) no-repeat center top / cover`}}
                    >
                        {
                            !props.loggedOwner.avatar && props.loggedOwner.name[0]
                        }
                        
                    </div>
                    <span className="Navbar__User-Menu-Name">{props.loggedOwner.name}</span>
                    {/* <button className="Navbar__User-Menu-Dropdown-Button">d</button> */}
                    {
                        isDropdownOpened && <DropdownMenu />
                        
                    }
                </div>
                :
                <div className="Navbar__Auth-Buttons">
                    <NavLink 
                        to="/login" 
                        className="Navbar__Auth-Button"
                    >Sign In</NavLink>
                    <NavLink 
                        to="/signup" 
                        className="Navbar__Auth-Button Navbar__Auth-Button_Highlighted" 
                    >Sign Up</NavLink>
                </div>
            }
            
        </div>
        {
            (isLoginPopupOpened && !isSignUpPopupOpened) 
            ?
            <LoginPopup />
            :
            (isSignUpPopupOpened && !isLoginPopupOpened) 
            ?
            <SignUpPopup />
            :
            null
        }
    </>);
};

const mapStateToProps = (state: State): StateProps => (
    {
        loggedOwner: state.loggedOwner
    }
);

export default connect(mapStateToProps)(Navbar);