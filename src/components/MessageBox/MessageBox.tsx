import React, { useState, useCallback } from 'react';
import './MessageBox.scss';

type Props = {
    message: string,
    confirmative: boolean
    onOkPressed?: (EO: React.MouseEvent<HTMLButtonElement>) => void
    onCancelPressed?: () => void
};

function MessageBox(props: Props) {
    
    return <div className="Message-Box">
        <p className="Message-Box__Message-Text">{props.message}</p>
        <button 
            className="Message-Box__Button Message-Box__Button_Yes"
            onClick={props.onOkPressed}       
        >Да</button>
        <button 
            className="Message-Box__Button Message-Box__Button_No"
            onClick={props.onCancelPressed}       
        >Нет</button>
    </div>;
};

export default MessageBox;