import React, { useState, useRef, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { State, Owner } from '../../types/types';
import { fetchLoggedOwner, editOwner } from '../../actions/ownersActions';
import { getLocalStorageItem } from '../../modules/localStorageAPI';
import { Redirect, NavLink } from 'react-router-dom';

export type InputElement = HTMLInputElement | HTMLSelectElement | HTMLTextAreaElement;


function UserSettings() {
    const dispatch = useDispatch();

    const localStorageLoggedUserData = getLocalStorageItem("loggedUser");
    const localStorageDoesntHaveDataAboutLoggedUser = !localStorageLoggedUserData;

    const loggedOwner: Owner | null = useSelector((state: State) => state.loggedOwner);
    const DEFAULT_AVATAR_PATH = "http://cdn.onlinewebfonts.com/svg/img_343322.png";
    
    const [tempAvatar, setTempAvatar] = useState<any>(DEFAULT_AVATAR_PATH);
    const [payload, setPayload] = useState({...loggedOwner});
    const [isSubmitButtonDisabled, setIsSubmitButtonDisabled] = useState(true);


    useEffect(() => {
       setTempAvatar(loggedOwner?.avatar);
       setPayload(loggedOwner as Owner);
    }, [loggedOwner?.id])


    
    
    const emailRef = useRef<HTMLInputElement>(null);
    // const passwordRef = useRef<HTMLInputElement>(null);
    // const confirmPasswordRef = useRef<HTMLInputElement>(null);
    const nameRef = useRef<HTMLInputElement>(null);
    const locationRef = useRef<HTMLInputElement>(null);

    const editPayload = (EO: React.ChangeEvent<InputElement>) => {
        const prop = EO.target.dataset.field as string;
        setPayload({...payload, [prop]: EO.target.value});
        setIsSubmitButtonDisabled(false);
    }

    const handleAvatarInputChange = (EO: React.ChangeEvent<HTMLInputElement>) => {
        console.log(EO);
        let file = EO.target.files && EO.target.files[0];
        let reader = new FileReader();

        if (file) {
            reader.readAsDataURL(file);
        }

        reader.onload = function() {
            setTempAvatar(reader.result);
            setIsSubmitButtonDisabled(false);
        };

        reader.onerror = function() {
            console.log(reader.error);
        };
    };

    const submitEditedUserData = async (EO: React.FormEvent<HTMLFormElement>) => {
        EO.preventDefault();
        setIsSubmitButtonDisabled(true);
        await dispatch(editOwner(loggedOwner?.key as string, payload as Owner, tempAvatar));
    };
    const clearTempAvatar = (EO: React.MouseEvent<HTMLButtonElement>) => {
        EO.preventDefault(); 

        setTempAvatar(DEFAULT_AVATAR_PATH);
        setIsSubmitButtonDisabled(false);

    }

    if (localStorageDoesntHaveDataAboutLoggedUser) {
        return <Redirect to="/" />
    }
    

    if (!loggedOwner) {
        return <div>Loading...</div>
    }
    return <form onSubmit={submitEditedUserData}>
        <div className="Manage-Avatar">
            {
                loggedOwner.avatar !== ""
                ?
                <img style={{width: "200px"}} className="Manage-Avatar__Avatar" src={tempAvatar} alt="Avatar"/>
                :
                <img style={{width: "200px"}} className="Manage-Avatar__Avatar" src={DEFAULT_AVATAR_PATH} alt="Avatar"/>
            }
            <div className="Manage-Avatar__Buttons">
                <label className="Manage-Avatar__Avatar-File-Label">
                    Загрузить
                    <input 
                        type="file"
                        className="Manage-Avatar__Avatar-File-Input"
                        onChange={handleAvatarInputChange}
                    />
                </label>
                <button 
                    className="Manage-Avatar__Button Manage-Avatar__Button_Delete"
                    onClick={clearTempAvatar}    
                >Удалить</button>
            </div>
        </div>

        <label className="Registration-Input-Container__Label">E-mail : <br />
            <input 
                className="Registration-Input-Container__Input" 
                defaultValue={loggedOwner.email}
                onChange={editPayload}
                ref={emailRef}
                data-field="email"
                type="email" 
                placeholder="E-mail" 
                required 
            />
        </label>
        <label className="Registration-Input-Container__Label">Name : <br />
            <input 
                className="Registration-Input-Container__Input" 
                defaultValue={loggedOwner.name}
                onChange={editPayload}
                ref={nameRef}
                data-field="name"
                type="text" 
                required 
            />
        </label>
        <label className="Registration-Input-Container__Label">Location : <br />
            <input 
                className="Registration-Input-Container__Input" 
                ref={locationRef}
                onChange={editPayload}
                defaultValue={loggedOwner.location}
                data-field="location"
                type="text" 
            />
        </label>
        <input type="submit" disabled={isSubmitButtonDisabled} value="Submit" />
        <NavLink to="/profile">Cancel</NavLink>
    </form>;

};


export default UserSettings;