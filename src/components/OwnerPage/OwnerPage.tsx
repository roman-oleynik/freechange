import React, {useState, useEffect} from 'react';
import {connect} from 'react-redux';
import { Owner, Action, State } from '../../types/types';
import {fetchOwner, setOwner} from '../../actions/ownersActions';
import { NavLink } from 'react-router-dom';
import { ThunkDispatch } from 'redux-thunk';
import { bindActionCreators } from 'redux';
import './OwnerPage.scss';


type Match = {
    path: string,
    url: string,
    isExact: boolean,
    params: {
      ownerId: string
    }
};

interface StateProps {
    owner: Owner
};
  
interface DispatchProps {
    fetchOwner: (ownerId: string) => void
    setOwner: (owner: Owner) => void
};
  
interface OwnProps {
    match: Match
};
  
type Props = StateProps & DispatchProps & OwnProps;



function OwnerPage(props: Props) {
    const ownerId = props.match.params.ownerId;
    useEffect(() => {
        props.fetchOwner(ownerId);
        return () => {
            props.setOwner({} as Owner);
        };
    }, [ownerId]);

    return <div className="Owner-Page">
        <h1 className="Owner-Page__Title">{props.owner.name}</h1>
        <img className="Owner-Page__Avatar" src={props.owner.avatar} alt="Avatar"/>
    </div>
}

const mapStateToProps = (state: State): StateProps => (
    {
        owner: state.owner
    }
);
const mapDispatchToProps = (dispatch: ThunkDispatch<{}, {}, Action>) => bindActionCreators(
    {
        fetchOwner,
        setOwner
    },
        dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(OwnerPage);